function add(n1, n2) {
	return n1 + n2;
}

function subtract(n1, n2) {
	return n1 - n2;
}

function multiply(n1, n2) {
	return n1 * n2;
}

let n1 = 5;
let n2 = 7;

var product = multiply(n1,n2);

console.log("The sum of " + n1 + " and " + n2 + " is: " + add(n1,n2));
console.log("The difference of " + n1 + " and " + n2 + " is: " + subtract(n1,n2));
console.log("The product of " + n1 + " and " + n2 + " is: " + product);