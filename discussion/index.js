document.getElementById("btn-1").addEventListener('click', () => { alert("Add more!");});

let paragraph = document.getElementById("paragraph-1");
let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn-2").addEventListener('click', () => {paragraph.innerHTML = "I can even do this!";});

document.getElementById("btn-3").addEventListener('click', () => {
	paragraph2.innerHTML = "Or this!";
	paragraph2.style.color = "red";
	paragraph2.style.fontSize = "50px";
});

console.log("hello world");

let my_bands = ["Beatles", "Stereopony", "Rythem", "Rivermaya", "Eraserheads"];

let person = {
	firstName : "Enrico Miguel",
	lastName : "Silvano",
	isDeveloper : true,
	hasPortfolio : true,
	age : 24
};

console.log("My fav bands:" + " " + my_bands[0] + " " + my_bands[1] + " " + my_bands[2] + " " + my_bands[3] + " " + my_bands[4]);

console.log("");
console.log("person object details");
console.log(person.firstName);
console.log(person.lastName);
console.log(person.isDeveloper);
console.log(person.hasPortfolio);
console.log(person.age);